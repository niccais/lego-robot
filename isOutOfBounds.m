function [result] = isOutOfBounds(LysData)

if LysData > 620
    result = 1;
else
    result = 0;
end

end