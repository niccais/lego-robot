function onClose(src,evt)
% When user tries to close the figure, end the while loop and
% dispose of the figure
global running;
running = 0;
delete(src);
end