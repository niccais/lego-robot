%% Lego Robot
%
% Group 1326
%
% Program for navigating NXT robot with a joystick

handle_NXT = handshake();

%% Initialize process
joymex2('open',0);                  % Open Joystick
mV  = initMotor('C');               % Left motor
mH  = initMotor('B');               % Right motor
OpenLight(SENSOR_1, 'ACTIVE');      % Light sensor

%% Variables
%%% Joystick X,Y array
[jX,jY]                 = deal(zeros(1,100));     % Joystick x and y position

%%% Arrays
[Lys, Areals,...
    Deriverte, Filter,...
    AvvikV]             = deal(zeros(1,100));

%%% Constants
global running;
global i;
lysNullVerdi            = GetLight(SENSOR_1);
outOfBounds             = 0;
dataLimit               = 20;   % How detailed should the filter be 
                                % (higher value = more cut-off)
%%% Start values
js                          = joymex2('query',0);    % Fetch joystick data
[running, i]                = deal(1);

%% Plots
% Set default units for when applying figure position.
set(0,'DefaultFigureUnits','normalized')
figure('Position',[0.1,0.1,0.8,0.7], 'CloseRequestFcn',@onClose, 'name','fig1');

%%% Power bar
subplot(2,1,1)

bV = bar(1,[0],'r'); hold on
bH = bar(2,[0],'g');
set(gca,'xtick',1:2)
axis([0 3 -100 100])
title('Pĺdrag Hřyre og Venstre Motor');

%%% Deviation
subplot(2,1,2);
p1 = plot([0], [0], 'or');
xlim([-100 100]);
title('Avvik fra sentrum av banen');

%% The Loop
while (~js.buttons(1) && running)
    %% Fetch data
    %%% Joystick
    js                  = joymex2('query',0);      % Update joystick data
    jX(i)               = -js.axes(2)/327.68;      % Joystick x-axes inverted, -100:100
    jY(i)               = js.axes(1)/327.68;       % Joystick y-axes, -100:100
    
    %%% Time
    Time(i)             = tic;
    
    %%% Sensors
    Lys(i)              = GetLight(SENSOR_1);        % Get new light data
    if i==3
        lysNullVerdi    = Lys(i);
    end
    
    %% Calculations
    %%% Light filter
    % Collected light data is filtered to produce a smoother curve.
    Filter(i) = filterData(Lys,dataLimit);
    
    %%% Deviation
    % Calculate deviation from initial light scanned.
    AvvikV(i)   = Filter(i)-lysNullVerdi;   %EGET PLOT FOR DETALJERT VISNING AV AVVIK
    Avvik       = abs(Filter(i)-lysNullVerdi);
    
    
    %Dersom lysdata er innenfor rammene til lysnullverdi:
    %Behold forrige areal
    Areals(i) = intLys(Areals(i-1), AvvikV, Time);
    
    %%% Engine
    [mV, mH] = engineHandler(round(jX(i)/2), round(jY(i)/3), mH, mV);
    
    %% Update plots
    %%% Power bar
    set(bV, 'Ydata', mV.Power);
    set(bH, 'Ydata', mH.Power);
    
    %%% Deviation
    set(p1, 'Xdata', AvvikV(i));
    
    %% End of loop
    drawnow
    i = i + 1;
end
save saved.mat
if running
    onClose(1,0);
end

%%% Calculate final score
calcScore(Areals(i-1),toc(Time(1)));

%%% Close connections and figures
terminateNXT(mV, mH, handle_NXT);
close(findobj('type','figure','name','fig1'));