function handle_NXT = handshake

%% Forberedelser for MATLAB
COM_CloseNXT all                % lukker alle NXT-h�ndtak
close all                       % lukker alle figurer
clear all                       % sletter alle variable

%% etablere kontakt med NXT
handle_NXT = COM_OpenNXT();     % etablerer nytt h�ndtak
COM_SetDefaultNXT(handle_NXT);	% setter globalt standard-h�ndtak

end