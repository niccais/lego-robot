function integr = intLys(old, value, time)

global i;

if i > 1
    integr = old + value(i)*(toc(time(i-1)));
else
    integr = value(i)*toc(time(i));
end

end