function [mV, mH] = engineHandler(Power, Turn, mH, mV)

    Turn = round(Turn);
    if (Power + Turn) > 100;
        Turn = 100 - Power;
    elseif (Turn - Power) < -100
        Turn = Power - 100;
    end
    mH.Power = Power - Turn;
    mV.Power = Power + Turn;
    save engine.mat
    mH.SendToNXT();
    mV.SendToNXT();

end